sudo docker run -d -p 9080:9000 \
    --name portainer \
    --restart always \
    -v /opt/portainer/portainer_data:/data \
    -v "/var/run/docker.sock:/var/run/docker.sock" \
    portainer/portainer \
