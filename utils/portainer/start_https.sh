sudo docker run -d -p 9443:9000 \
    --name portainer \
    --restart always \
    -v /etc/ssl/certs:/certs \
    -v /opt/portainer/portainer_data:/data \
    -v "/var/run/docker.sock:/var/run/docker.sock" \
    portainer/portainer \
    --ssl --sslcert /certs/fullchain.pem --sslkey /certs/private.key \
